"use client"
import { ApolloClient, InMemoryCache } from "@apollo/client";

const client = new ApolloClient({
    uri: "https://main--spotify-demo-graph-3sd44s.apollographos.net/graphql",
    cache: new InMemoryCache(),
});

export default client;
