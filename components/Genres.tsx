"use client"
import {useQuery} from "@apollo/client";
import { Artists } from "@/constants/constants";
import styled from "styled-components";


const GenresWrapper = styled.div`
  width: 1200px;
  display: flex;
  flex-wrap: wrap;

`;


const Genre = styled.div`
  border-radius: 12px;
  background-color: teal;
  color: aliceblue;
  padding: 24px 12px;
  width: 25%;
  margin-right: 18px;
  &:not(:first-child) {
    margin-top: 24px;
  }
`

function GenresComponent() {
    const { data } = useQuery(Artists);

    return (
        <GenresWrapper>{data?.genres?.map((genre: any, index: number) => <Genre key={index}>{genre}</Genre>)}</GenresWrapper>
    );
}

export default GenresComponent;
