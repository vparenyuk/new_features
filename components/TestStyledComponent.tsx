"use client"

import styled from "styled-components";

const Styled = styled.h1`
    color: teal;
    font-size: 24px;
  
`

function TestStyledComponent() {
    return (
        <Styled>
            Styled Text
        </Styled>
    );
}

export default TestStyledComponent;
